dj-database-url==0.5.0
Django==2.0.6
gunicorn==19.8.1
psycopg2-binary==2.8.4
pytz==2019.3
SQLAlchemy==1.2.10
whitenoise==3.3.1
pillow
coverage
    