# Predict Phone Price (Intelligent Systems Group Assignment)
Machine Learning

## Contents
1. [Group Members](#group-members)
2. [Links](#links)

## Group Members
- Wan Muhammad Rayhan Arwindra (1806241210)
- Muhammad Farhan Oktavian (1806173512)
- Figo Muhammad Alfaritzi (1806241072)
- Ahmad Zahir Rabbani (1806241002)

## Links
- [Dataset](https://gitlab.com/rayhan.arwindra/sistem-cerdas/-/raw/master/data/mobile.csv) - ```https://gitlab.com/rayhan.arwindra/sistem-cerdas/-/raw/master/data/mobile.csv```
- [Prototype](https://www.figma.com/file/pGeRDPmpSejMqPUPeIeWxQ/Sistem-Cerdas-PPP?node-id=0%3A1) - ```https://www.figma.com/file/pGeRDPmpSejMqPUPeIeWxQ/Sistem-Cerdas-PPP?node-id=0%3A1```
- [Heroku](http://pypp.herokuapp.com/) - ```http://pypp.herokuapp.com/```
