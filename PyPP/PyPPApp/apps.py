from django.apps import AppConfig


class PyppappConfig(AppConfig):
    name = 'PyPPApp'
