from django.shortcuts import render, redirect
from . import views
from .dt.decisiontree import predict
from .forms import *
def show_result(request):
    return render(request, 'coba.html' , {'result' : result})

def index(request):
    form = PPForm(request.POST or None)
    if form.is_valid():
        data = {
        'battery_power' : form.cleaned_data['battCapacity'],
        'blue' : form.cleaned_data['bluetooth'],
        'clock_speed' : form.cleaned_data['clockSpeed'],
        'dual_sim' : form.cleaned_data['dualSim'],
        'fc' : form.cleaned_data['frontCamera'],
        'four_g' : form.cleaned_data['fourG'],
        'int_memory' : form.cleaned_data['memorySize'],
        'm_dep' : form.cleaned_data['thickness'],
        'mobile_wt' : form.cleaned_data['weight'],
        'n_cores' : form.cleaned_data['coreNumber'],
        'pc' : form.cleaned_data['primaryCamera'],
        'px_height' : form.cleaned_data['resHeight'],
        'px_width' : form.cleaned_data['resWidth'],
        'ram' : form.cleaned_data['ramSize'],
        'sc_h' : form.cleaned_data['screenHeight'],
        'sc_w' : form.cleaned_data['screenWidth'],
        'talk_time' : form.cleaned_data['talkTime'],
        'three_g' : form.cleaned_data['threeG'],
        'touch_screen' : form.cleaned_data['touchScreen'],
        'wifi' : form.cleaned_data['wifi']
        }
        result = {'result' : predict(data)}
        #show_result: nama view, result
        #ini redirect gw belom tau bener ngga
        return render(request, 'result.html', result)
    elif request.method=="GET":
        return render(request, 'base.html', {'form':form})
