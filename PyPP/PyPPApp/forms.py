from django import forms


class PPForm(forms.Form):
    touchScreen = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    clockSpeed = forms.DecimalField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    coreNumber = forms.ChoiceField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
        (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], widget=forms.Select({
            'class': 'custom-select',
        }))
    ramSize = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    memorySize = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    battCapacity = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    talkTime = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    thickness = forms.DecimalField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    weight = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    screenHeight = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    screenWidth = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    resHeight = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    resWidth = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    primaryCamera = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    frontCamera = forms.IntegerField(widget=forms.NumberInput({
        'class': 'form-control',
    }))
    bluetooth = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    wifi = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    dualSim = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    fourG = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    threeG = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
